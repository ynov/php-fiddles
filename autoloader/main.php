#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Foo\Example\Person;
use Foo\Example\Bar\Hello;

$person = new Person();

$person->id = 1;
$person->name = "Foo";
$person->age = 20;
$person->address = "Here St. 5";

var_dump($person);

Hello::println('The quick brown fox jumps over the lazy dog');

echo 'ROOT_DIR: ' . Hello::rootDir() . "\n\n";

echo "Hello::renderView('hello.json'):\n";
Hello::renderView('hello.json');

echo "\n";

echo "Hello::renderView('hello.json'):\n";
Hello::renderView('not_found.json');
