<?php

namespace Foo\Example\Bar;

class Hello
{
    public static function println($line)
    {
        echo "${line}\n";
    }

    public static function rootDir()
    {
        return ROOT_DIR;
    }

    public static function renderView($viewName)
    {
        try {
            if (!file_exists(VIEWS_DIR . "/{$viewName}")) {
                throw new \Exception("View doesn't exist!");
            }

            include(VIEWS_DIR . "/{$viewName}");
        } catch (\Exception $e) {
            echo "ERROR: {$e->getMessage()}\n";
            return false;
        }

        return true;
    }
}
