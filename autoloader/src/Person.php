<?php

namespace Foo\Example;

class Person
{
    protected $id;
    protected $name;
    protected $age;
    protected $address;

    public function __set($key, $val)
    {
        $this->{$key} = $val;
    }

    public function __get($key)
    {
        return $this->{$key};
    }
}
