#!/usr/bin/env php
<?php
require __DIR__ . '/lib/linkedlist.php';
use LinkedList\LinkedList;

//////////////////////////////////////////////////////////////////////////////
$list = new LinkedList();

$list->push('hello');
$list->push('world');
$list->push(75 * 20);
$list->push('foo bar qux');

//////////////////////////////////////////////////////////////////////////////
echo "==== Using normal while loop:\n";

$i = 0;
$node = $list->getFirstNode();
while ($node != null) {
    $element = $node->getElement();
    ++$i;
    echo "{$i}. {$element}\n";

    $node = $node->getNext();
}

//////////////////////////////////////////////////////////////////////////////
echo "==== Using foreach:\n";

foreach ($list as $i => $element) {
    ++$i;
    echo "{$i}. {$element}\n";
}

//////////////////////////////////////////////////////////////////////////////
echo '==== list.isEmpty(): ' . ($list->isEmpty() ? 'true' : 'false') . "\n";
echo "==== pop()-ing:\n";

while (!$list->isEmpty()) {
    $i = $list->getSize();
    $element = $list->pop();

    echo "{$i}. {$element}\n";
}

echo '==== list.isEmpty(): ' . ($list->isEmpty() ? 'true' : 'false') . "\n";
