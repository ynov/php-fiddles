<?php

trait MagicGetterAndSetter
{
    public function __call($funcName, $args)
    {
        if (strpos($funcName, 'get') === 0) {
            $property = lcfirst(str_replace('get', '', $funcName));
            return $this->{$property};
        } else if (strpos($funcName, 'set') === 0) {
            $property = lcfirst(str_replace('set', '', $funcName));
            $this->{$property} = $args[0];
        }
    }
}
