<?php
namespace LinkedList;

require_once __DIR__ . '/magic.php';

class Node
{
    use \MagicGetterAndSetter;

    protected $element;
    protected $previous;
    protected $next;

    public function __construct($element, $previous = null, $next = null)
    {
        $this->element = $element;
        $this->previous = $previous;
        $this->next = $next;
    }

    public function __toString()
    {
        return $this->element;
    }
}

class LinkedList implements \Iterator
{
    use \MagicGetterAndSetter;

    protected $size;
    protected $firstNode;
    protected $lastNode;

    protected $currentIndex = 0;
    protected $currentNode = null;

    public function __construct()
    {
        $this->size        = 0;
        $this->firstNode   = null;
        $this->lastNode    = null;
    }

    public function push($element)
    {
        $node = new Node($element);

        if ($this->size == 0) {
            $this->firstNode = $node;
            $this->lastNode = $node;
            $this->currentNode = $this->firstNode;
        } else {
            $node->setPrevious($this->lastNode);
            $this->lastNode->setNext($node);
            $this->lastNode = $node;
        }

        $this->size++;
    }

    public function pop()
    {
        $element = $this->lastNode->getElement();

        $this->lastNode = $this->lastNode->getPrevious();
        if ($this->lastNode !== null) {
            $this->lastNode->setNext(null);
        }

        $this->size--;
        return $element;
    }

    public function isEmpty()
    {
        return $this->size == 0;
    }

    // Implement iterator functions
    public function rewind()
    {
        $this->currentNode = $this->firstNode;
        $this->currentIndex = 0;
    }

    public function current()
    {
        return $this->currentNode->getElement();
    }

    public function key()
    {
        return $this->currentIndex;
    }

    public function next()
    {
        $this->currentNode = $this->currentNode->getNext();
        $this->currentIndex++;
    }

    function valid()
    {
        return $this->currentNode !== null;
    }
}
