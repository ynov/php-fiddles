<?php
function readln($prompt = null)
{
    if ($prompt != null) {
        echo "{$prompt}";
    }
    return trim(fgets(STDIN));
}
