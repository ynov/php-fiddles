#!/usr/bin/env php
<?php

require __DIR__ . '/mydb.php';

$dsn = 'mysql:dbname=qux;host=127.0.0.1';
$username = 'foo';
$password = readln("Password for 'foo': ");

$db = new MyDB($dsn, $username, $password);

$input = [];
$user  = [];
$SQL  = [];

$SQL['login'] =
    'SELECT * FROM users WHERE username = :username AND password = :password';

$SQL['create_table_posts'] =
    'CREATE TABLE IF NOT EXISTS posts ('
        . ' id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,'
        . ' user_id INTEGER UNSIGNED NOT NULL,'
        . ' created_at DATETIME,'
        . ' content TEXT,'
        . ' FOREIGN KEY (user_id)'
        . ' REFERENCES users(id)'
        . ' ON DELETE CASCADE'
    . ')';

$SQL['create_post'] =
    'INSERT INTO posts (user_id, created_at, content)'
        . ' VALUES(:user_id, :created_at, :content)';

$SQL['delete_post'] =
    'DELETE FROM posts WHERE id = :id AND user_id = :user_id';

$SQL['all_posts'] =
    'SELECT posts.*, users.username FROM posts'
        . ' LEFT JOIN users ON (posts.user_id = users.id)';

$SQL['my_posts'] =
    'SELECT posts.*, users.username FROM posts'
        . ' LEFT JOIN users ON (posts.user_id = users.id)'
        . ' WHERE posts.user_id = :user_id';

// [0]
// $sql = "DROP TABLE IF EXISTS posts";
// $db->exec($sql);
// readln();

// [1]
$db->exec($SQL['create_table_posts']);
readln();

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

function login()
{
    global $db, $SQL, $user, $input;

    echo "Login\n==========\n";
    $input = [];
    $input['username'] = readln('Username: ');
    $input['password'] = sha1(readln('Password: '));

    $user = $db->queryOne($SQL['login'],
                          [':username' => $input['username'],
                           ':password' => $input['password']]);

    if (!$user) {
        echo "Login failed.\n";
        die();
    } else {
        echo "Login success.\n";
    }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

function createPost()
{
    global $db, $SQL, $user, $input;

    $input['content'] = readln("Input your post's content: ");

    $db->exec($SQL['create_post'],
              [':user_id'    => $user['id'],
               ':created_at' => date('Y-m-d H:i:s'),
               ':content'    => $input['content']]);

    echo "Post created!\n";
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

function deletePost()
{
    global $db, $SQL, $user, $input;

    $input['post_id'] = readln("Post ID: ");

    $rowCount = $db->exec($SQL['delete_post'],
                          [':id'      => $input['post_id'],
                           ':user_id' => $user['id']]);

    if ($rowCount > 0) {
        echo "Post deleted!\n";
    } else {
        echo "Couldn't delete post!\n";
    }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

function listAllPosts()
{
    global $db, $SQL;

    $posts = $db->queryAll($SQL['all_posts']);

    echo "All Posts:\n";

    foreach ($posts as $post) {
?>==============================================================================
ID: <?php echo $post['id'] . "\n"; ?>
By: <?php echo $post['username']; ?> at <?php echo $post['created_at'] . "\n"; ?>
Content: <?php echo $post['content'] . "\n"; ?>
==============================================================================

<?php
    }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

function listMyPosts()
{
    global $db, $SQL, $user;

    $posts = $db->queryAll($SQL['my_posts'],
                           [':user_id' => $user['id']]);

    echo "My Posts:\n";

    foreach ($posts as $post) {
?>==============================================================================
ID: <?php echo $post['id'] . "\n"; ?>
By: <?php echo $post['username']; ?> at <?php echo $post['created_at'] . "\n"; ?>
Content: <?php echo $post['content'] . "\n"; ?>
==============================================================================

<?php
    }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

login();

while (true) {

    echo <<<EOF
1. List All Posts
2. List My Posts
3. Create a Post
4. Delete a Post
EOF;

    $choice = readln("\nYour choice: ");
    if (!preg_match('/\d+/', $choice)) {
        echo "Invalid choice.\n";
        readln();
        continue;
    }

    switch($choice) {
    case 1:
        listAllPosts();
        break;
    case 2:
        listMyPosts();
        break;
    case 3:
        createPost();
        break;
    case 4:
        deletePost();
        break;
    default:
        echo "Invalid choice.\n";
    }

    readln();
}
