#!/usr/bin/env php
<?php

require __DIR__ . '/mydb.php';

$dsn = 'mysql:dbname=qux;host=127.0.0.1';
$username = 'foo';
$password = readln("Password for 'foo': ");

$db = new MyDB($dsn, $username, $password);

// [1]
$sql = <<<SQL
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS users;
SQL;
$db->exec($sql);
readln();

// [2]
$sql = <<<SQL
CREATE TABLE users (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    address VARCHAR(255) DEFAULT 'N/A'
);
SQL;
$db->exec($sql);
readln();

// [3]
$sql = <<<SQL
INSERT INTO users (username, password, email)
    VALUES ('foo', SHA1('foo123'), 'foo@example.com');
INSERT INTO users (username, password, email, address)
    VALUES ('bar', SHA1('bar123'), 'bar@example.com', 'Hover St.');
INSERT INTO users (username, password, email, address)
    VALUES ('qux', SHA1('qux123'), 'qux@example.com', 'Money Rd.');
SQL;
$db->exec($sql);
readln();

// [4]
$all = $db->queryAll("SELECT * FROM users");
print_r($all);
readln();

// [5]
$user = $db->queryOne("SELECT email, address FROM users WHERE username = :username",
                      [':username' => 'bar']);
print_r($user);
readln();

// [6]
$user = $db->queryOne("SELECT username FROM users WHERE id = ?", [3]);
print_r($user);
readln();

// [7]
$user = $db->queryOne("SELECT * FROM users WHERE username = :username AND password = :password",
                      [':username' => 'bar',
                       ':password' => sha1('bar123')]);
print_r($user);
readln();

// [8]
$user = $db->queryOne("SELECT * FROM users WHERE username = :username AND password = :password",
                      [':username' => 'foo',
                       ':password' => sha1('wrong_password')]);
print_r($user);
echo '$user == null ? ' . ($user == null ? 'true' : 'false') . "\n";
readln();

$db->close();
