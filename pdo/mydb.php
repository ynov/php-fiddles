<?php

function readln($str = "...\n")
{
    if (!empty($str)) echo $str;
    return trim(fgets(STDIN));
}

class MyDB
{
    protected $dbh = null;

    public function __construct($dsn, $username, $password)
    {
        try {
            $this->dbh = new PDO($dsn, $username, $password);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Connection failed: {$e->getMessage()}\n";
            die();
        }
    }

    public function exec($sql, $attrs = [], $print = true)
    {
        $result = null;

        if ($print) echo rtrim($sql) . "\n";
        if (!empty($attrs)) {
            echo ' `-- attrs: [ ';
            foreach ($attrs as $k => $v) {
                echo $k . ' => ' . $v . ', ';
            }
            echo " ]\n";
        }

        $sth = $this->dbh->prepare($sql);

        try {
            $sth->execute($attrs);
            $result = $sth->rowCount();
        } catch (Exception $e) {
            echo "ERROR: {$e->getMessage()}\n";
            return false;
        }

        return $result;
    }

    protected function queryX($sql, $attrs = [], $print = true, $all = true)
    {
        if ($print) echo rtrim($sql) . "\n";
        if (!empty($attrs)) {
            echo ' `-- attrs: [ ';
            foreach ($attrs as $k => $v) {
                echo $k . ' => ' . $v . ', ';
            }
            echo " ]\n";
        }

        $result = null;
        $sth = $this->dbh->prepare($sql);

        try {
            $sth->execute($attrs);
            $result = $all ? $sth->fetchAll() : $sth->fetch();
        } catch (Exception $e) {
            echo "ERROR: {$e->getMessage()}\n";
            return false;
        }

        return $result;
    }

    public function queryAll($sql, $attrs = [], $print = true)
    {
        return $this->queryX($sql, $attrs, $print, true);
    }

    public function queryOne($sql, $attrs = [], $print = true)
    {
        return $this->queryX($sql, $attrs, $print, false);
    }

    public function close()
    {
        $this->dbh = null;
    }
}
