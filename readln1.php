#!/usr/bin/env php
<?php

require __DIR__ . '/lib/readln.php';

$line = readln('Your name: ');
echo "Hello, {$line}!\n";

$line = readln('Array: ');
$array = preg_split('/[\s]+/', $line);
print_r($array);
