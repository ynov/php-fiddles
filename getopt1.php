#!/usr/bin/env php
<?php

$sopts = 'n:';
// $sopts = '';
$lopts = ['name:'];

$options = getopt($sopts, $lopts);

if (!count($options)) {
    echo "Usage: getopt1.php --name <name>\n";
    exit(0);
}

$name = isset($options['n']) ? $options['n'] : $options['name'];
echo "Hello, ${name}!\n";
